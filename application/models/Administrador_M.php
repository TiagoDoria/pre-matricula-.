<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrador_M extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	public function cadastro_disciplinas($data)
	{
		

		$query =  $this->db->get_where('disciplina',array('codigo'=>$data['codigo']));
		$result = $query->result_array();
	
		if(count($result) > 0) { 
			$this->session->set_flashdata('acaoform', 'Disciplina já cadastrada!.'); 
			redirect('Administrador/cadastrar_disciplinas');

		}
		return $this->db->insert('disciplina',$data);  
			
	}	

	public function deletar_disciplina($id=null){
		$this->db->where('id',$id);
		
		return $this->db->delete('disciplina');

	}

	public function editar()
	{
		$id = $this->input->post('id');
		$data['nome'] = $this->input->post('nome');
		$data['codigo'] = $this->input->post('codigo');
		$data['carga_horaria'] = $this->input->post('carga_horaria');
		$data['horario'] = $this->input->post('horario');
		$data['modulo'] = $this->input->post('modulo');
		$data['ementa'] = $this->input->post('ementa');

		$this->db->where('id',$id);

		//esses numeros sao para verificar se foi possivel atualizar  ou nao
		return $this->db->update('disciplina',$data);		

	}



}
