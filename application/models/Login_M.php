<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_M extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}

	public function logar_usuario($registration,$password)
	{
		
		$this->db->where('senha',md5($password));
		$this->db->where('matricula',$registration);

		$data['estudantes'] = $this->db->get('estudantes')->result();

		//count = contar a quantidade de registros que satisfazem a condição acima
		if(count($data['estudantes']) == 1){ // existe
			
			$dados['nome'] = $data['usuario'][0]->nome;
			$dados['id'] = $data['usuario'][0]->id;
			$dados['logado'] = true;
			$this->session->set_userdata($dados);
			redirect('home');

		}
		$email = $this->input->post('matricula');
		
		$this->db->where('senha',$password);
		$this->db->where('email',$email);

		$data['administrador'] = $this->db->get('administrador')->result();
		if(count($data['administrador']) == 1){ // existe
			
			$dados['nome'] = $data['usuario'][0]->nome;
			$dados['id'] = $data['usuario'][0]->id;
			$dados['logado'] = true;
			$this->session->set_userdata($dados);
			redirect('home_admin');
		}else{
			echo  "<script>alert('Usuario nao encontrado!.');location.href='index';</script>";
			
		}
	}
}
