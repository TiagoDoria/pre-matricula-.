<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disciplina_M extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	public function listar(){

		$this->db->select('*'); // seleciona todos os dados da tabela
		return $this->db->get('disciplina')->result();

	}

	public function procurar($cod){

		$this->db->select('nome'); // seleciona todos os dados da tabela
		$this->db->where('codigo',$cod);
		return $this->db->get('disciplina')->result();

	}

	public function qtd_disciplinas()
	{
		$this->db->select('count(*) as total'); // contando qtd de registros na tabela
		return $this->db->get('disciplina')->result();
	}	

	public function listar_disc($value,$qtd_disc){

		$this->db->select('*'); // seleciona todos os dados da tabela
		$this->db->limit($qtd_disc,$value);
		return $this->db->get('disciplina')->result();

	}

	public function pesquisar_codigo()
	{
		$busca = $this->input->post('pesquisarcodigo');
		$this->db->select('*'); // seleciona todos os dados da tabela
		$this->db->like('codigo',$busca);
		return $this->db->get('disciplina')->result();
	}

	public function pesquisar_nome()
	{
		$busca = $this->input->post('pesquisarnome');
		$this->db->select('*'); // seleciona todos os dados da tabela
		$this->db->like('nome',$busca);
		return $this->db->get('disciplina')->result();
	}

	public function solicitar_matricula($data){

		$query =  $this->db->get_where('solicitacao',array('codigo'=>$data['codigo']));
		$result = $query->result_array();
		if(count($result) > 0) { 
			$this->session->set_flashdata('acaoform', 'Solicitação  já realizada!.'); 
			redirect('Estudante/paginacao');

		}

		return $this->db->insert('solicitacao',$data); 
	}

	public function matricula($busca)
	{
		


	}
 

}
