	<title>Home</title>

	<div class="col-sm-9 col-sm-offset-3 col-md-12 col-md-offset-2 main"  style"=border:2px #333">
        <div class="col-md-10">

          <div class="table-responsive">
            <table class="table table-striped"  border="1">
              <thead>
                <tr>
                  <th>
                    <h1 class="page-header" style="text-align:center;">Bem Vindo!</h1>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <p>
                      No período de 01 a 04 de novembro de 2016, a UFBA disponibilizará a pré-matrícula online dos seus alunos. 
                      Esse processo visa contribuir para que seja possível verificar a demanda de turmas a ser disponibilizadas 
                      em determinadas disciplinas.
                    </p><br>
                      
                    <p>
                      A realização da pré-matrícula não garante o direito a cursar a disciplina no semestre pleiteado, 
                      cabendo ao colegiado o direito de não disponibilizar as matérias que não obtiverem o número mínimo de matriculados.
                    </p><br>
          
                    <p>
                      Vale lembrar que a matrícula na WEB ocorrerá nos dias 08 a 10 de novembro de 2016.
                    </p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>  
        </div>  
      </div>

       
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="<?= base_url(); ?>/assets/js/bootstrap.min.js"></script>
    
    <script src="<?= base_url(); ?>/assets/js/vendor/holder.min.js"></script>
  
    <script src="<?= base_url(); ?>/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
