    <title>Editar disciplina</title>

     <?php
         $acaoflash = $this->session->flashdata('acaoform');    
         if (isset($acaoflash) && $acaoflash!=''){
           echo "<script>alert('".$acaoflash."')</script>";
         }
     ?>

    <div class="container">
    </br></br>
      <form class="form-signin" id="meuform" method="post" action="<?= base_url() ?>Administrador/editar">
        <h2 class="form-signin-heading">Editar Disciplina</h2></br>
      
        <input type="hidden" id="id" name="id" value="<?= $disciplina[0]->id; ?>">
        <label class="sr-only">Nome</label>
        <input type="text" id="inputNome" name="nome" class="form-control" value="<?= $disciplina[0]->nome; ?>" required autofocus></br>

        <label class="sr-only">Codigo</label>
        <input type="text" id="inputDepartamento" name="codigo" class="form-control" value="<?= $disciplina[0]->codigo; ?>" required autofocus></br>
      
        <label class="sr-only">Carga Horaria</label>
        <input type="text" id="inputHora" name="carga_horaria" class="form-control" value="<?= $disciplina[0]->carga_horaria; ?>" required autofocus></br>
       
        <label class="sr-only">Horario</label>
        <input type="text" id="inputDepartamento" name="horario" class="form-control" value="<?= $disciplina[0]->horario; ?>" required autofocus></br>
       
        <label class="sr-only">Modulo</label>
        <input type="text" id="inputRequisito" name="modulo" class="form-control" value="<?= $disciplina[0]->modulo; ?>" required autofocus></br> 


        <textarea rows="10" class="form-control" cols="30"  name="ementa"><?php echo $disciplina[0]->ementa; ?></textarea></br>
        </br>
        <input type="submit" class="btn btn-lg btn-success btn-block" value="Editar">
      </form>
     
    </div>    
    <script src="<?= base_url(); ?>/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>

