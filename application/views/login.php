
  <title>Sistema Pré-matricula</title>
  <?php
    $acaoflash = $this->session->flashdata('acaoform');    
    if (isset($acaoflash) && $acaoflash!=''){
        echo "<script>alert('".$acaoflash."')</script>";
    }
  ?>

 
    <div class="container">

      <form class="form-signin" method="post" action="<?= base_url(); ?>Login/logar_usuario" >
        <h2 class="form-signin-heading">Login</h2></br>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="text" id="inputMatricula" name="matricula" class="form-control" placeholder="Matricula" required autofocus></br>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="senha"  class="form-control" placeholder="Senha" required></br>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button></br>
        <a href="<?= base_url(); ?>Estudante/register_student" class="btn btn-lg btn-success btn-block">Cadastre-se</a>
      </form>

    </div> 


   
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
