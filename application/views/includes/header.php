
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url(); ?>assets/img/favicon.ico">
   
    <link href="<?= base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
    
    <link href="<?= base_url(); ?>/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
   
    <link href="<?= base_url(); ?>/assets/css/style.css" rel="stylesheet">

    <script src="<?= base_url(); ?>/assets/js/ie-emulation-modes-warning.js"></script>

   
  </head>

  <body>