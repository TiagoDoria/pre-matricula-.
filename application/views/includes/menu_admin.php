<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?= base_url(); ?>Administrador ">Sistema Pre-matricula</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?= base_url(); ?>Home_admin/logout">Logout</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="<?= base_url(); ?>Administrador">Home</a></li>
            <li><a href="<?= base_url(); ?>Administrador/cadastrar_disciplinas">Cadastrar Disciplina</a></li>
            <li><a href="<?= base_url(); ?>Administrador/paginacao">Listar Disciplinas</a></li>
            <li><a href="#">Resultado</a></li>
            <li><a href="#">Resultado final</a></li>
            <li><a href="#"></a></li>
          </ul>
         
        </div>	
