      <title>Listar disciplinas</title>   



        <div class="col-sm-9 col-sm-offset-3 col-md-12 col-md-offset-2 main">
          <div class="col-md-10">
            <h1 class="page-header" style="text-align:center;">Disciplinas disponíveis</h1>
              <div class="table-responsive">
                 <div class="col-md-3" style="margin-left:-10px;">
                        <form action="<?= base_url(); ?>Estudante/pesquisar_codigo" method="post">
                          <div class="form-group">
                            <label>Código:</label>
                            <input type="text" class="form-control" id="pesquisarcodigo" name="pesquisarcodigo" placeholder="Pesquisar...">
                            <button type="submit" class="btn btn-sucess">Pesquisar</button> 
                          </div>
                        </form>
                      </div>

                     <div class="col-md-3" style="margin-left:-10px;">
                        <form action="<?= base_url(); ?>Estudante/pesquisar_nome" method="post">
                          <div class="form-group">
                            <label>Nome:</label>
                            <input type="text" class="form-control" id="pesquisarnome" name="pesquisarnome" placeholder="Pesquisar...">
                            <button type="submit" class="btn btn-sucess">Pesquisar</button> 
                          </div>
                        </form>
                      </div>

                      <?php
                    $acaoflash = $this->session->flashdata('acaoform');    
              if (isset($acaoflash) && $acaoflash!=''){
              echo "<script>alert('".$acaoflash."')</script>";
          }
            ?>   

            <script language="JavaScript">
             function marcardesmarcar(){
                $('.marcar').each(
                function(){
                  if ($(this).prop( "checked")) 
                    $(this).prop("checked", false);
                  else $(this).prop("checked", true);               
                }
              );
              }
            </script>
                  
               <form method="post" action="<?= base_url(); ?>Estudante/solicitar_matricula" >       
                <table class="table table-striped"  border="1">
                  <thead>
                    <tr>
                      <th>Código</th>
                      <th>Nome</th>
                      <th>Carga horária</th>
                      <th>Vagas</th>
                      <th>Horário</th>
                      <th>Ementa</th>
                      <th>

                      </th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php $i = 0; ?>
                      <?php foreach ($disciplinas as $dis) { ?>
                       
                      <tr>
                        <td><?= $dis->codigo; ?></td>
                        <td><?= $dis->nome; ?></td> 
                        <td><?= $dis->carga_horaria; ?></td>
                        <td><?= $dis->modulo; ?></td>
                        <td><?= $dis->horario; ?></td>
                        <td><?= $dis->ementa; ?></td>
                      
                        <td>
                          <label><input type='checkbox'  id='todos' onclick='marcardesmarcar();' name='check_codigo'  value="<?= $dis->codigo; ?>"/></label>
                        <!--  <label><input type='checkbox'  class='marcar' name='check_nome' value="<?= $dis->nome; ?>"   /></label>
                          <label><input type='checkbox'  class='marcar' name="check_carga" value="<?= $dis->carga_horaria; ?>"  /></label>
                          <label><input type='checkbox'  class='marcar' name="check_ementa" value="<?= $dis->ementa; ?>"  /></label> -->
                        </td>
                       
                      </tr>
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td> 
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                      <?php } ?>  
                   

                    
                    

                    <!-- $usuarios é uma variavel qualquer definida na hora para printar os dados --> 
                  </tbody>   
                  
                </table>
                  <button type="submit" class="btn btn-sucess">Enviar</button> 

                </form>

               

              

                 <nav aria-label="Page navigation">
                  <ul class="pagination">
                    <!-- <li>
                       <a href="<?= base_url('Estudante/paginacao/'.($value-$qtd_disc)); ?>" aria-label="Anterior" style="<?= $btnA ?>">
                         <span aria-hidden="true">&laquo;</span>
                      </a>
                    </li> -->
                    <?php 
                    
                    $num_pag = 0;
                    for($i=0; $i<$qtd_botao; $i++){  ?>
                      <li><a href="<?= base_url('Estudante/paginacao/'.$num_pag); ?>"><?= $i?></a></li>
                    <?php 
                      $num_pag += $qtd_disc;
                    } ?> 

                   <!-- <li>
                      <a href="<?= base_url('Estudante/paginacao/'.($value+$qtd_disc)); ?>" aria-label="Próximo" style="<?= $btnP ?>">
                      <span aria-hidden="true">&raquo;</span>
                      </a>
                    </li> -->
                  </ul>
                </nav>
              </div>
        

          <div class="col-md-12">

          </div>
          

         
         
    </div>
    </div>
    </div>

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?= base_url(); ?>assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>  
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/vendor/holder.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
