    <title>Cadastrar disciplina</title>

     <?php
         $acaoflash = $this->session->flashdata('acaoform');    
         if (isset($acaoflash) && $acaoflash!=''){
           echo "<script>alert('".$acaoflash."')</script>";
         }
     ?>

    <div class="container">
    </br></br>
      <form class="form-signin" id="meuform" method="post" action="<?= base_url() ?>Administrador/cadastro_disciplinas">
        <h2 class="form-signin-heading">Cadastrar Disciplina</h2></br>
      
        <label class="sr-only">Nome</label>
        <input type="text" id="inputNome" name="nome" class="form-control" placeholder="Nome" required autofocus></br>

        <label class="sr-only">Codigo</label>
        <input type="text" id="inputDepartamento" name="codigo" class="form-control" placeholder="Codigo" required autofocus></br>
      
        <label class="sr-only">Carga Horaria</label>
        <input type="text" id="inputHora" name="carga_horaria" class="form-control" placeholder="Carga Horaria" required autofocus></br>
       
        <label class="sr-only">Horario</label>
        <input type="text" id="inputDepartamento" name="horario" class="form-control" placeholder="Horario" required autofocus></br>
       
        <label class="sr-only">Modulo</label>
        <input type="text" id="inputRequisito" name="modulo" class="form-control" placeholder="Modulo" required autofocus></br> 

         <label class="sr-only">Ementa</label>
        <textarea name="ementa" rows="4" cols="34" form="meuform" placeholder="Ementa da disciplina"></textarea></br>
        </br>
        <input type="submit" class="btn btn-lg btn-success btn-block" value="Cadastrar">
      </form>
     
    </div>    
    <script src="<?= base_url(); ?>/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>

