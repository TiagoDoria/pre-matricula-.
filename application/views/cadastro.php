 
   <title>Cadastro</title>

    <div class="container">
    </br></br>
      <form class="form-signin" action="<?= base_url(); ?>Estudante/registering_student" method="post">
        <h2 class="form-signin-heading">Cadastro</h2></br>
      
        <font color="grey" size="4px">Nome</font>
        <label class="sr-only">Nome</label>
        <input type="text" id="inputNome" class="form-control" name="nome"  placeholder="Nome" required autofocus></br>
      
        <font color="grey" size="4px">Email</font>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email" required autofocus></br>
       
        <font color="grey" size="4px">Curso</font>
        <label for="inputCurso" class="sr-only">Curso</label>
        <input type="text" id="inputCurso" class="form-control" name="curso" placeholder="Curso" required autofocus></br>
       
        <font color="grey" size="4px">Matricula</font>
        <label for="inputMatricula" class="sr-only">Matricula</label>
        <input type="text" id="inputMatricula" class="form-control" name="matricula" placeholder="Matricula" required autofocus></br>

        <font color="grey" size="4px">Senha</font>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" name="senha" placeholder="Senha" required></br>

        <font color="grey" size="4px">Repetir Senha</font>
        <label for="inputRepeatPassword" class="sr-only">Password</label>
        <input type="password" id="inputRepeatPassword" class="form-control" name="repetirsenha" placeholder="Repetir Senha" required></br></br>
        
        <input type="submit" class="btn btn-lg btn-success btn-block" value="Cadastrar">
      </form>

    </div> 

    <?php
    $acaoflash = $this->session->flashdata('acaoform');    
    if (isset($acaoflash) && $acaoflash!=''){
        echo "<script>alert('".$acaoflash."')</script>";
    }
?>


   
    <script src="<?= base_url(); ?>/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
