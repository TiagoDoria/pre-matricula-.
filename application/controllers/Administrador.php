<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrador extends CI_Controller {

	public function check_sess()
	{

		if($this->session->userdata('logado') == false ){
			redirect('home/login');
		}
	}
	
	
	public function index()
	{
		$this->check_sess();
		$this->load->view('includes/header');
		$this->load->view('includes/menu_admin');
		$this->load->view('home_admin');
	}

	public function listar_disciplinas()
	{
		$this->check_sess();
		$this->load->model('Disciplina_M','disciplina');
		$dados['disciplinas'] = $this->disciplina->listar();
		$this->load->view('includes/header');
		$this->load->view('includes/menu_admin');
		$this->load->view('listar_disciplina_admin',$dados); // enviando os dados da tabela para a view
		

	}

	public function cadastrar_disciplinas()
	{
		$this->check_sess();
		$this->load->view('includes/header');
		$this->load->view('includes/menu_admin');
		$this->load->view('cadastro_disciplina');

	}

	public function cadastro_disciplinas()
	{
		$data['nome'] = $this->input->post('nome');
		$data['codigo'] = $this->input->post('codigo');
		$data['carga_horaria'] = $this->input->post('carga_horaria');
		$data['horario'] = $this->input->post('horario');
		$data['modulo'] = $this->input->post('modulo');
		$data['ementa'] = $this->input->post('ementa');
		
		 $this->load->model('Administrador_M','adm');
		 if($this->adm->cadastro_disciplinas($data)){  
			$this->session->set_flashdata('acaoform', 'Cadastrado com sucesso!.');  
			redirect('home_admin');
		}
	}

	public function editar_disciplina($id=null){
		$this->check_sess();

		$this->db->where('id',$id);
		$data['disciplina'] = $this->db->get('disciplina')->result();
		$this->load->view('includes/header');
		$this->load->view('includes/menu_admin');
		$this->load->view('editar_disciplina',$data);

	}

	public function deletar_disciplina($id=null){
		$this->load->model('Administrador_M','adm');
		if($this->adm->deletar_disciplina($id)){
			redirect('Administrador/paginacao');

		}else{
			redirect('Administrador/paginacao');
		}

	}

	public function editar($value=null)
	{

		if($value == null){
			$value = 0; // pagina 1	
		}

		$qtd_disc = 10; // 10 disciplinas por pagina

		if($value <= $qtd_disc){
			$data['btnA'] = 'pointer-events: none'; // não há outra pagina alem da 1, entao deve esta desativada
		}
		else{
			$data['btnA'] = '';
		}

		$this->load->model('Disciplina_M','dis');
		$query = $this->dis->qtd_disciplinas();

		if(($query[0]->total - $value) < $qtd_disc){
			$data['btnP'] = 'pointer-events: none';
		}
		else{
			$data['btnP'] = '';
		}

		$this->load->model('Disciplina_M','dis');
		$data['disciplinas'] = $this->dis->listar_disc($value,$qtd_disc);

		$data['value'] = $value;
		$data['qtd_disc'] = $qtd_disc;
		$data['qtd_pag'] = $query[0]->total;

		$qtd = (int) $query[0]->total/$qtd_disc;
		$resto = $query[0]->total%$qtd_disc;

		
		$data['qtd_botao'] = $qtd;


		$this->load->model('Administrador_M','adm');
		//esses numeros sao para verificar se foi possivel atualizar  ou nao
		if($this->adm->editar()){
			$this->load->view('includes/header');
			$this->load->view('includes/menu_admin');
			$this->load->view('listar_disciplina_admin',$data);

		}else{
			$this->session->set_flashdata('acaoform', 'Não foi possível editar!.'); 
			redirect('Administrador/paginacao');
		}

	}

	public function paginacao($value=null)
	{
		if($value == null){
			$value = 0; // pagina 1	
		}

		$qtd_disc = 10; // 10 disciplinas por pagina

		if($value <= $qtd_disc){
			$data['btnA'] = 'pointer-events: none'; // não há outra pagina alem da 1, entao deve esta desativada
		}
		else{
			$data['btnA'] = '';
		}

		$this->load->model('Disciplina_M','dis');
		$query = $this->dis->qtd_disciplinas();

		if(($query[0]->total - $value) < $qtd_disc){
			$data['btnP'] = 'pointer-events: none';
		}
		else{
			$data['btnP'] = '';
		}

		$this->load->model('Disciplina_M','dis');
		$data['disciplinas'] = $this->dis->listar_disc($value,$qtd_disc);

		$data['value'] = $value;
		$data['qtd_disc'] = $qtd_disc;
		$data['qtd_pag'] = $query[0]->total;

		$qtd = (int) $query[0]->total/$qtd_disc;
		$resto = $query[0]->total%$qtd_disc;

		
		$data['qtd_botao'] = $qtd;

		$this->load->view('includes/header');
		$this->load->view('includes/menu_admin');
		$this->load->view('listar_disciplina_admin',$data); 

	}	

	public function pesquisar_codigo($value=null)
	{
		
		
		if($value == null){
			$value = 0; // pagina 1	
		}

		$qtd_disc = 10; // 10 disciplinas por pagina

		if($value <= $qtd_disc){
			$data['btnA'] = 'pointer-events: none'; // não há outra pagina alem da 1, entao deve esta desativada
		}
		else{
			$data['btnA'] = '';
		}

		$this->load->model('Disciplina_M','dis');
		$query = $this->dis->qtd_disciplinas();

		if(($query[0]->total - $value) < $qtd_disc){
			$data['btnP'] = 'pointer-events: none';
		}
		else{
			$data['btnP'] = '';
		}

		$this->load->model('Disciplina_M','dis');
		$data['disciplinas'] = $this->dis->listar_disc($value,$qtd_disc);

		$data['value'] = $value;
		$data['qtd_disc'] = $qtd_disc;
		$data['qtd_pag'] = $query[0]->total;

		$qtd = (int) $query[0]->total/$qtd_disc;
		$resto = $query[0]->total%$qtd_disc;

		
		$data['qtd_botao'] = $qtd;
		
		
		$this->load->model('Disciplina_M','dis');
		$data['disciplinas'] = $this->dis->pesquisar_codigo();

		$this->load->view('includes/header');
		$this->load->view('includes/menu_admin');
		$this->load->view('listar_disciplina_admin',$data);


	}		

	public function pesquisar_nome($value=null)
	{
		if($value == null){
			$value = 0; // pagina 1	
		}

		$qtd_disc = 10; // 10 disciplinas por pagina

		if($value <= $qtd_disc){
			$data['btnA'] = 'pointer-events: none'; // não há outra pagina alem da 1, entao deve esta desativada
		}
		else{
			$data['btnA'] = '';
		}

		$this->load->model('Disciplina_M','dis');
		$query = $this->dis->qtd_disciplinas();

		if(($query[0]->total - $value) < $qtd_disc){
			$data['btnP'] = 'pointer-events: none';
		}
		else{
			$data['btnP'] = '';
		}

		$this->load->model('Disciplina_M','dis');
		$data['disciplinas'] = $this->dis->listar_disc($value,$qtd_disc);

		$data['value'] = $value;
		$data['qtd_disc'] = $qtd_disc;
		$data['qtd_pag'] = $query[0]->total;

		$qtd = (int) $query[0]->total/$qtd_disc;
		$resto = $query[0]->total%$qtd_disc;

		
		$data['qtd_botao'] = $qtd;
		
		
		$this->load->model('Disciplina_M','dis');
		$data['disciplinas'] = $this->dis->pesquisar_nome();

		$this->load->view('includes/header');
		$this->load->view('includes/menu_admin');
		$this->load->view('listar_disciplina_admin',$data);


	}		

	

}
