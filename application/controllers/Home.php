<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function check_sess()
	{

		if($this->session->userdata('logado') == false ){
			redirect('home/login');
		}
	}

	
	public function index()
	{
		$this->check_sess();
		$this->load->view('includes/header');
		$this->load->view('includes/menu');
		$this->load->view('home');
	}

	// carrega a view login
	public function login()
	{
		$this->load->view('includes/header');
		$this->load->view('login');
	}


	public function logout(){
		$this->session->sess_destroy();
		redirect('home');

	}
}
