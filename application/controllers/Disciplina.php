<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disciplina extends CI_Controller {

	public function check_sess()
	{

		if($this->session->userdata('logado') == false ){
			redirect('home/login');
		}
	}
	
	public function index2()
	{
		$this->load->view('includes/menu');
		$this->load->view('includes/header');
		$this->load->view('cadastrodisciplina');
	}


	public function listar_disciplinas()
	{
		$this->check_sess();
		$this->load->model('Disciplina_M','disciplina');
		$dados['disciplinas'] = $this->disciplina->listar();
		$this->load->view('includes/header');
		$this->load->view('includes/menu_admin');
		$this->load->view('listar_disciplina_admin',$dados); // enviando os dados da tabela para a view
		

	}

	

	/* value indica a pagina atual, se clicarmos na pagina 5 por exenplo 
	a pagina 5 deve esta desativada.
	a pagina ira mostrar 10 disciplinas por pagina*/
	public function paginacao($value=null)
	{
		if($value == null){
			$value = 0; // pagina 1	
		}

		$qtd_disc = 10; // 10 disciplinas por pagina

		if($value <= $qtd_disc){
			$data['btnA'] = 'pointer-events: none'; // não há outra pagina alem da 1, entao deve esta desativada
		}
		else{
			$data['btnA'] = '';
		}

		$this->load->model('Disciplina_M','dis');
		$query = $this->dis->qtd_disciplinas();

		if(($query[0]->total - $value) < $qtd_disc){
			$data['btnP'] = 'pointer-events: none';
		}
		else{
			$data['btnP'] = '';
		}

		$this->load->model('Disciplina_M','dis');
		$data['disciplinas'] = $this->dis->listar_disc($value,$qtd_disc);

		$data['value'] = $value;
		$data['qtd_disc'] = $qtd_disc;
		$data['qtd_pag'] = $query[0]->total;

		$qtd = (int) $query[0]->total/$qtd_disc;
		$resto = $query[0]->total%$qtd_disc;

		
		$data['qtd_botao'] = $qtd;

		$this->load->view('includes/header');
		$this->load->view('includes/menu_admin');
		$this->load->view('listar_disciplina_admin',$data); 

	}	

	public function index()
	{
		// Instancia a classe mPDF
		$mpdf = new mPDF();
		// Ao invés de imprimir a view 'welcome_message' na tela, passa o código
		// HTML dela para a variável $html
		$html = $this->load->view('listar_disciplinas','',TRUE);
		// Define um Cabeçalho para o arquivo PDF
		$mpdf->SetHeader('Gerando PDF no CodeIgniter com a biblioteca mPDF');
		// Define um rodapé para o arquivo PDF, nesse caso inserindo o número da
		// página através da pseudo-variável PAGENO
		$mpdf->SetFooter('{PAGENO}');
		// Insere o conteúdo da variável $html no arquivo PDF
		$mpdf->writeHTML($html);
		// Cria uma nova página no arquivo
		$mpdf->AddPage();
		// Insere o conteúdo na nova página do arquivo PDF
		$mpdf->WriteHTML('<p><b>Minha nova página no arquivo PDF</b></p>');
		// Gera o arquivo PDF
		$mpdf->Output();
	}	




}
