<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function check_sess()
	{

		if($this->session->userdata('logado') == false ){
			redirect('home/login');
		}
	}

	public function index()
	{
		$this->check_sess();
		$this->load->view('includes/header');
		$this->load->view('login');
	}


	public function logar_usuario()
	{
		$registration = $this->input->post('matricula');
		$password = $this->input->post('senha');
		$this->load->model('Login_M','login');
		$this->login->logar_usuario($registration,$password);
	}	
}
