<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_admin extends CI_Controller {

	public function check_sess()
	{

		if($this->session->userdata('logado') == false ){
			redirect('home_admin/login');
		}
	}

	
	public function index()
	{
		$this->check_sess();
		$this->load->view('includes/header');
		$this->load->view('includes/menu_admin');
		$this->load->view('home_admin');
	}

	// carrega a view login

	
	public function logout(){
		$this->session->sess_destroy();
		redirect('Home');

	}
}
