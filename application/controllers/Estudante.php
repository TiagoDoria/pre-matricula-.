<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estudante extends CI_Controller {

  public function check_sess()
  {

    if($this->session->userdata('logado') == false ){
      redirect('home/login');
    }
  }
  
  public function index()
  {
    $this->check_sess();
    $this->load->view('includes/header');
    $this->load->view('includes/menu');
    $this->load->view('home');
  }

  //função carrega a view do cadastro
  public function register_student()
  {
    $this->load->view('includes/header');
    $this->load->view('cadastro');
  }

  public function listar_disciplinas()
  {
    
    $this->check_sess();
    $this->load->model('Disciplina_M','disciplina');
    $dados['disciplinas'] = $this->disciplina->listar();
    $this->load->view('includes/header');
    $this->load->view('includes/menu');
    $this->load->view('listar_disciplinas',$dados);
    
  }

  //cadastre estudante, insere dados o banco
  public function registering_student()
  {
    $data['nome'] = $this->input->post('nome');
    $data['email'] = $this->input->post('email');
    $data['curso'] = $this->input->post('curso');
    $data['matricula'] = $this->input->post('matricula');
    $data['senha'] = md5($this->input->post('senha'));
    $verificar = md5($this->input->post('repetirsenha'));

    if($verificar != $data['senha']){
      $this->session->set_flashdata('acaoform', 'Senhas não correspondentes!.'); 
      redirect('Estudante/register_student');

    }

    $query =  $this->db->get_where('estudantes',array('email'=>$data['email']));
    $result = $query->result_array();
    $query =  $this->db->get_where('estudantes',array('matricula'=>$data['matricula']));

    $result2 = $query->result_array();
    if(count($result) > 0) { 
      $this->session->set_flashdata('acaoform', 'Email já cadastrado!.'); 
      redirect('Estudante/register_student');

    }else if(count($result2) > 0) { 
      $this->session->set_flashdata('acaoform', 'Estudante já cadastrado!.'); 
      redirect('Estudante/register_student');

    } 
    
    $this->load->model('Estudante_M','estudante');
    if($this->estudante->registering_student($data)){  
      $this->session->set_flashdata('acaoform', 'Cadastrado com sucesso!.');  
      redirect('home/login');

    }else{
      $this->session->set_flashdata('acaoform', 'Erro ao cadastrar!.'); 
      redirect('Estudante/register_student');
    }


  }

  public function paginacao($value=null)
  {
    if($value == null){
      $value = 0; // pagina 1 
    }

    $qtd_disc = 10; // 10 disciplinas por pagina

    if($value <= $qtd_disc){
      $data['btnA'] = 'pointer-events: none'; // não há outra pagina alem da 1, entao deve esta desativada
    }
    else{
      $data['btnA'] = '';
    }

    $this->load->model('Disciplina_M','dis');
    $query = $this->dis->qtd_disciplinas();

    if(($query[0]->total - $value) < $qtd_disc){
      $data['btnP'] = 'pointer-events: none';
    }
    else{
      $data['btnP'] = '';
    }

    $this->load->model('Disciplina_M','dis');
    $data['disciplinas'] = $this->dis->listar_disc($value,$qtd_disc);

    $data['value'] = $value;
    $data['qtd_disc'] = $qtd_disc;
    $data['qtd_pag'] = $query[0]->total;

    $qtd = (int) $query[0]->total/$qtd_disc;
    $resto = $query[0]->total%$qtd_disc;

    
      
    $data['qtd_botao'] = $qtd;

    $this->load->view('includes/header');
    $this->load->view('includes/menu');
    $this->load->view('listar_disciplinas',$data); 

  } 


  public function pesquisar_codigo($value=null)
  {
    
    
    if($value == null){
      $value = 0; // pagina 1 
    }

    $qtd_disc = 10; // 10 disciplinas por pagina

    if($value <= $qtd_disc){
      $data['btnA'] = 'pointer-events: none'; // não há outra pagina alem da 1, entao deve esta desativada
    }
    else{
      $data['btnA'] = '';
    }

    $this->load->model('Disciplina_M','dis');
    $query = $this->dis->qtd_disciplinas();

    if(($query[0]->total - $value) < $qtd_disc){
      $data['btnP'] = 'pointer-events: none';
    }
    else{
      $data['btnP'] = '';
    }

    $this->load->model('Disciplina_M','dis');
    $data['disciplinas'] = $this->dis->listar_disc($value,$qtd_disc);

    $data['value'] = $value;
    $data['qtd_disc'] = $qtd_disc;
    $data['qtd_pag'] = $query[0]->total;

    $qtd = (int) $query[0]->total/$qtd_disc;
    $resto = $query[0]->total%$qtd_disc;

    
    $data['qtd_botao'] = $qtd;
    
    
    $this->load->model('Disciplina_M','dis');
    $data['disciplinas'] = $this->dis->pesquisar_codigo();

    $this->load->view('includes/header');
    $this->load->view('includes/menu');
    $this->load->view('listar_disciplinas',$data);


  }   

  public function pesquisar_nome($value=null)
  {
    if($value == null){
      $value = 0; // pagina 1 
    }

    $qtd_disc = 10; // 10 disciplinas por pagina

    if($value <= $qtd_disc){
      $data['btnA'] = 'pointer-events: none'; // não há outra pagina alem da 1, entao deve esta desativada
    }
    else{
      $data['btnA'] = '';
    }

    $this->load->model('Disciplina_M','dis');
    $query = $this->dis->qtd_disciplinas();

    if(($query[0]->total - $value) < $qtd_disc){
      $data['btnP'] = 'pointer-events: none';
    }
    else{
      $data['btnP'] = '';
    }

    $this->load->model('Disciplina_M','dis');
    $data['disciplinas'] = $this->dis->listar_disc($value,$qtd_disc);

    $data['value'] = $value;
    $data['qtd_disc'] = $qtd_disc;
    $data['qtd_pag'] = $query[0]->total;

    $qtd = (int) $query[0]->total/$qtd_disc;
    $resto = $query[0]->total%$qtd_disc;

    
    $data['qtd_botao'] = $qtd;
    
    
    $this->load->model('Disciplina_M','dis');
    $data['disciplinas'] = $this->dis->pesquisar_nome();

    $this->load->view('includes/header');
    $this->load->view('includes/menu');
    $this->load->view('listar_disciplinas',$data);


  }   


  public function solicitar_matricula($value=null)
  {

    if($value == null){
      $value = 0; // pagina 1 
    }

    $qtd_disc = 10; // 10 disciplinas por pagina

    if($value <= $qtd_disc){
      $data['btnA'] = 'pointer-events: none'; // não há outra pagina alem da 1, entao deve esta desativada
    }
    else{
      $data['btnA'] = '';
    }

    $this->load->model('Disciplina_M','dis');
    $query = $this->dis->qtd_disciplinas();

    if(($query[0]->total - $value) < $qtd_disc){
      $data['btnP'] = 'pointer-events: none';
    }
    else{
      $data['btnP'] = '';
    }

    $this->load->model('Disciplina_M','dis');
    $data['disciplinas'] = $this->dis->listar_disc($value,$qtd_disc);

    $data['value'] = $value;
    $data['qtd_disc'] = $qtd_disc;
    $data['qtd_pag'] = $query[0]->total;

    $qtd = (int) $query[0]->total/$qtd_disc;
    $resto = $query[0]->total%$qtd_disc;

    
    $data['qtd_botao'] = $qtd;
    
    

    $check['codigo'] = $this->input->post('check_codigo');
    $busca = $check['codigo'];

    
    if($check['codigo'] == null)
    {
      $this->session->set_flashdata('acaoform', 'favor, selecione uma disciplina!.');  
      redirect('Estudante/paginacao');
    }
    else{
      $data = $this->db->get_where('disciplina' , array('codigo' => $busca ))->row();
      $check['nome'] = $data->nome;
      $check['carga_horaria'] = $data->carga_horaria;
      $check['ementa'] = $data->ementa;
    }

    $this->load->model('Disciplina_M','estudante');
       
    
    if($this->estudante->solicitar_matricula($check)){ 
  
      $this->session->set_flashdata('acaoform', 'Solicitação realizada com sucesso!.');  
      redirect('Estudante/paginacao');  
    

    }else{
      $this->session->set_flashdata('acaoform', 'Erro ao solicitar!.'); 
      redirect('Estudante/paginacao');
    }

  }

  


  


}
